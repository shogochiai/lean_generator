package yakolab.s4m.util
{
	public class Vec
	{
		public var x:Number;
		public var y:Number;
		
		public function Vec(_x:Number = 0, _y:Number = 0)
		{
			this.x = _x;
			this.y = _y;
		}
	}
}